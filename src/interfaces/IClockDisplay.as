package interfaces
{
	public interface IClockDisplay
	{
		function plus(i:int):String;
		function minus(i:int):String;
	}
}