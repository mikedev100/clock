package
{
	import flash.globalization.DateTimeFormatter;
	
	import interfaces.IClockDisplay;

	public class Clock extends Object
	{
		
		public function Clock()
		{
			date = new Date();
			dateFormatter = new DateTimeFormatter("en-gb");
			dateFormatter.setDateTimePattern("HH:mm");
			dateFormatter.format(date);
			super();
		}
		
		protected var date:Date;
		protected var dateFormatter:DateTimeFormatter
		public function at(hour:int, min:int=0):Clock
		{
			date.hours = hour;
			date.minutes = min;
			trace(dateFormatter.format(date));
			(dateFormatter.format(date) as Object);
			trace(this);
			return this;
		}
		
		public function plus(i:int):String
		{
			date.time += i*60*1000;
			return dateFormatter.format(date);
		}
		public function minus(i:int):String
		{
			date.time -= i*60*1000;
			return dateFormatter.format(date);
		}
		public function getTime():String
		{
			return dateFormatter.format(date);
		}
		public function toString():String
		{
			return dateFormatter.format(date);
		}
		public function equals(clock:Clock):Boolean
		{
			var returnBool:Boolean;
			if(this==clock)
			{
				returnBool = true;
			}
			return returnBool;
		}
	}
}